/* scrypt.c
 *
 * Scrypt password-based key derivation function.
 */

/* nettle, low-level cryptographics library
 *
 * Copyright (C) 2012 Simon Josefsson
 *
 * The nettle library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 *
 * The nettle library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the nettle library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02111-1301, USA.
 */

#if HAVE_CONFIG_H
# include "config.h"
#endif

#include <assert.h>
#include <stdlib.h>
#include <string.h>

#include "scrypt.h"

#include "pbkdf2.h"
#include "memxor.h"
#include "salsa20.h"
#include "macros.h"
#include "nettle-internal.h"

static void
_scryptBlockMix (uint32_t r, uint8_t *B, uint8_t *tmp2)
{
  uint64_t i;
  uint8_t *X = tmp2;
  uint8_t *Y = tmp2 + 64;

#if 0
  for (i = 0; i < 2 * r; i++)
    {
      size_t j;
      printf ("B[%d]: ", i);
      for (j = 0; j < 64; j++)
	{
	  if (j % 4 == 0)
	    printf (" ");
	  printf ("%02x", B[i * 64 + j]);
	}
      printf ("\n");
    }
#endif

  /* X = B[2 * r - 1] */
  memcpy (X, &B[(2 * r - 1) * 64], 64);

  /* for i = 0 to 2 * r - 1 do */
  for (i = 0; i <= 2 * r - 1; i++)
    {
      /* T = X xor B[i] */
      memxor(X, &B[i * 64], 64);

      /* X = Salsa (T) */
      _salsa20_core (X, X, 8);

      /* Y[i] = X */
      memcpy (&Y[i * 64], X, 64);
    }

  for (i = 0; i < r; i++)
    {
      memcpy (&B[i * 64], &Y[2 * i * 64], 64);
      memcpy (&B[(r + i) * 64], &Y[(2 * i + 1) * 64], 64);
    }

#if 0
  for (i = 0; i < 2 * r; i++)
    {
      size_t j;
      printf ("B'[%d]: ", i);
      for (j = 0; j < 64; j++)
	{
	  if (j % 4 == 0)
	    printf (" ");
	  printf ("%02x", B[i * 64 + j]);
	}
      printf ("\n");
    }
#endif
}

static void
_scryptROMix (uint32_t r, uint8_t *B, uint64_t N,
	      uint8_t *tmp1, uint8_t *tmp2)
{
  uint8_t *X = B, *T = B;
  uint64_t i;

#if 0
  printf ("B: ");
  for (i = 0; i < 128 * r; i++)
    {
      size_t j;
      if (i % 4 == 0)
	printf (" ");
      printf ("%02x", B[i]);
    }
  printf ("\n");
#endif

  /* for i = 0 to N - 1 do */
  for (i = 0; i <= N - 1; i++)
    {
      /* V[i] = X */
      memcpy (&tmp1[i * 128 * r], X, 128 * r);

      /* X =  ScryptBlockMix (X) */
      _scryptBlockMix (r, X, tmp2);
    }

  /* for i = 0 to N - 1 do */
  for (i = 0; i <= N - 1; i++)
    {
      uint64_t j;

      /* j = Integerify (X) mod N */
      j = LE_READ_UINT64 (&X[128 * r - 64]) % N;

      /* T = X xor V[j] */
      memxor (T, &tmp1[j * 128 * r], 128 * r);

      /* X = scryptBlockMix (T) */
      _scryptBlockMix (r, T, tmp2);
    }

#if 0
  printf ("B': ");
  for (i = 0; i < 128 * r; i++)
    {
      size_t j;
      if (i % 4 == 0)
	printf (" ");
      printf ("%02x", B[i]);
    }
  printf ("\n");
#endif
}

int
scrypt (unsigned passwdlen, const uint8_t * passwd,
	unsigned saltlen, const uint8_t * salt,
	uint64_t N, uint32_t r, uint32_t p,
	unsigned dkLen, uint8_t * DK)
{
  uint32_t i;
  uint8_t *B;
  uint8_t *tmp1;
  uint8_t *tmp2;

  printf ("N=%ld, r=%d, p=%d dkLen=%d\n", N, r, p, dkLen);

  /* XXX sanity-check parameters */

  B = malloc (p * 128 * r);
  if (B == NULL)
    return -1;

  tmp1 = malloc (N * 128 * r);
  if (tmp1 == NULL)
    return -1;

  tmp2 = malloc (64 + 128 * r);
  if (tmp2 == NULL)
    return -1;

  pbkdf2_hmac_sha256 (passwdlen, passwd, 1, saltlen, salt, p * 128 * r, B);

  for (i = 0; i < p; i++)
    _scryptROMix (r, &B[i * 128 * r], N, tmp1, tmp2);

  for (i = 0; i < p; i++)
    pbkdf2_hmac_sha256 (passwdlen, passwd, 1, p * 128 * r, B, dkLen, DK);

  free (tmp2);
  free (tmp1);
  free (B);

  return 0;
}
