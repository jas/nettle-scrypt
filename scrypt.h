/* scrypt.c
 *
 * Scrypt password-based key derivation function.
 */

/* nettle, low-level cryptographics library
 *
 * Copyright (C) 2012 Simon Josefsson
 *
 * The nettle library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 *
 * The nettle library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the nettle library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02111-1301, USA.
 */

#ifndef NETTLE_SCRYPT_H_INCLUDED
#define NETTLE_SCRYPT_H_INCLUDED

#include "nettle-meta.h"

#ifdef __cplusplus
extern "C"
{
#endif

/* Namespace mangling */
#define scrypt nettle_scrypt

int
scrypt (unsigned passwdlen, const uint8_t * passwd,
	unsigned saltlen, const uint8_t * salt,
	uint64_t N, uint32_t r, uint32_t p,
	unsigned dkLen, uint8_t * DK);

#ifdef __cplusplus
}
#endif

#endif /* NETTLE_SCRYPT_H_INCLUDED */
