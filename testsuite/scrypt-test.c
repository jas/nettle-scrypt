#include "testutils.h"
#include "scrypt.h"

void
test_main (void)
{
  char expect[] =
    "\x77\xd6\x57\x62\x38\x65\x7b\x20\x3b\x19\xca\x42\xc1\x8a\x04\x97"
    "\xf1\x6b\x48\x44\xe3\x07\x4a\xe8\xdf\xdf\xfa\x3f\xed\xe2\x14\x42"
    "\xfc\xd0\x06\x9d\xed\x09\x48\xf8\x32\x6a\x75\x3a\x0f\xc8\x1f\x17"
    "\xe8\xd3\xe0\xfb\x2e\x0d\x36\x28\xcf\x35\xe2\x0c\x38\xd1\x89\x06";
#define DKLEN 64
  uint8_t dk[DKLEN + 1];

  /* http://tools.ietf.org/html/draft-josefsson-scrypt-kdf */

  {
    uint32_t src[] = { 0x219a877e, 0x86c93e4f, 0xe640a97c, 0x268f7141,
		       0x5b55eeba, 0xb5c1618c, 0x1146f80d, 0x1d3bcd6d,
		       0x19f324ee, 0x853d9bdf, 0x4b1e1214, 0x32aac55a,
		       0x291d0276, 0x2948c709, 0x8dc6ebed, 0x5ec2b8b8 };
    uint32_t expect[] = { 0x9c851fa4, 0x99cc0866, 0xcbca813b, 0x05ef0c02,
			  0x81214b04, 0x7d33fda2, 0x631c7bfd, 0x292f6896,
			  0x683139b4, 0xbce6c9e3, 0xb7c56bfe, 0xba966da0,
			  0x10cc24e4, 0x5c74912c, 0x3d67ad24, 0x818f61c7 };
    uint32_t dst2[_SALSA20_INPUT_LENGTH];

    _salsa20_core (dst2, src, 8);
    ASSERT(MEMEQ (SALSA20_BLOCK_SIZE, dst2, expect));
  }

  dk[DKLEN] = 17;
  scrypt (0, "", 0, "", 16, 1, 1, DKLEN, dk);
  puts("got:");
  print_hex(DKLEN, dk);
  puts("expect");
  print_hex(DKLEN, expect);
  puts("done");
  ASSERT(MEMEQ (DKLEN, dk, expect));
  ASSERT(dk[DKLEN] == 17);

  scrypt (8, "password", 4, "NaCl", 1024, 8, 16, DKLEN, dk);
  puts("got:");
  print_hex(DKLEN, dk);

  scrypt (13, "pleaseletmein", 14, "SodiumChloride", 16384, 8, 1, DKLEN, dk);
  puts("got:");
  print_hex(DKLEN, dk);

  scrypt (13, "pleaseletmein", 14, "SodiumChloride", 1048576, 8, 1, DKLEN, dk);
  puts("got:");
  print_hex(DKLEN, dk);
}
